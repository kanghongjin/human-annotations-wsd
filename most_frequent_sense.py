import sqlite3
import itertools
import re
import random
import os
from collections import defaultdict

from mstranslator import mstranslator

import convert_to_ims_format

from IPython import embed
from google_translate_answer import translate


client_id = os.environ['TEST_MSTRANSLATOR_CLIENT_ID']
client_secret = os.environ['TEST_MSTRANSLATOR_CLIENT_SECRET']


use_google = True

translator = mstranslator.Translator(client_id, client_secret)

def get_unique_words(filename):
    unique_target_words = set()

    with open(filename, 'r') as input_file:
        for line in input_file:

            for token in line.split():
                if '#' not in token:
                    continue
                target_word = token.split('#')[0]
                unique_target_words.add(target_word)

    return list(unique_target_words)

if __name__ == "__main__":

    lexelt_counts = defaultdict(lambda : 0)

    with open('compiled_all.txt', 'r') as all_words_file,\
         open('most_frequent_translation_according_to_ge.txt', 'w+') as result_file:

        result = {}

        unique_words = get_unique_words('compiled_all.txt')
        
        if use_google is False:
            ts = translator.translate_array(unique_words, 'en' , 'zh')

            for word, t in itertools.izip(unique_words, ts):
                translations =  t['TranslatedText']
                result[word] = translations
        else:
            for word in unique_words:
                t = translate([ word ] )['translations'][0]
                print t
                text = t['translatedText']
                result[word] = text

        embed()

        for line in all_words_file:
            for token in line.split():
                if '#' not in token:
                    continue
                target_word = token.split('#')[0]
            try :
                bing_answer = result[target_word]
            except:
                bing_answer = 'U'

            if bing_answer != 'U':
                try:
                    bing_answer = convert_to_ims_format.get_senseid(bing_answer.encode('utf-8'))
                except KeyError:
                    bing_answer = 'OOV ' + bing_answer

            instance_id = target_word + '.' + str(lexelt_counts[target_word])
            result_file.write( "docsrc " + instance_id + " " + str(bing_answer)  + '\n')
            lexelt_counts[target_word] += 1





          