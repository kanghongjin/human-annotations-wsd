import os
import itertools
from collections import defaultdict, Counter
import glob
import csv
import sqlite3
import codecs
import re
import string
from IPython import embed
import nltk

"""
Running this script prints to stdout: the collated results in a format that the script "convert_to_ims_format.py" can process
The top of the file requires manual removal, if there are: the header like "--requires extra processing" will let you know...
That means that the instances has more than 3 annotations!

"""

context     = {}
target_word = {}

_config = {"directory" : "./csv_format/",
           "dictionary_sqlite" : "./dictionary_plus_naijia_ground_truth.db",
           'output_filename': 'compiled_all.txt',
           'remove_ne' : True,
           }

conn = sqlite3.connect(_config['dictionary_sqlite'])
c = conn.cursor()


_INSTANCE_ID_COL_NUM = 6

_pos_count = defaultdict(list)


def is_target_word_proper_noun(target_word, sentence, instance_id):
    sent1 = nltk.word_tokenize(sentence )

    sent2 = nltk.pos_tag(sent1)
    has_target = False
    for word, pos in sent2:
        if word.lower() != target_word.lower():
            continue
        _pos_count[pos].append(instance_id)
        has_target = True
        break


    if not has_target:
        raise ValueError(target_word + " not in  ... " + sentence)


    """
    sent3 =  nltk.ne_chunk(sent2, binary=True)

    for i in range(len(sent3)):
        #print str(sent3[i])
        if "NE" in str(sent3[i]) and sent3[i] == target_word:
            return True
    """
            
    return pos == "NNP"


def dictionary_answers_from_dictionary(english_word):
    c.execute("\
                SELECT chinese_meaning FROM english_words \
                INNER JOIN meanings \
                ON english_words.id = meanings.english_word_id \
                INNER JOIN chinese_words \
                ON meanings.chinese_word_id = chinese_words.id \
                WHERE english_meaning = ? \
              ", (english_word, ))
    return construct_set_from_database_result(c.fetchall())

def construct_set_from_database_result(db_result):
    result_as_list = map(lambda x: x[0], db_result)
    return frozenset(result_as_list)    

def out_of_vocab_annotations(candidate_words, english_word):
    dict_answers = map(lambda x : x.replace(" ", ""), dictionary_answers_from_dictionary(english_word))
    return list( set(candidate_words) - set(dict_answers) )

def instance_rows(csv_reader):
    """

    """
    instance = []

    times_first_cell_has_value = 0
    prev_returned_line_number = 0

    for i, row in enumerate(csv_reader):
        if row[0].strip() != "":
            times_first_cell_has_value += 1
            
        if times_first_cell_has_value % 2 != 0:

            if abs(i - prev_returned_line_number) > 1:
                yield (instance_id, instance)
                instance = []

            instance_id = row[_INSTANCE_ID_COL_NUM]

        else:
            instance.append(row)

def contexts(csv_filenames):
    results = {}
    for csv_filename in csv_filenames:
        with open(csv_filename, 'rb') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for instance_id, rows_of_instance in instance_rows(csv_reader):
                context = rows_of_instance[0][0]
                results[instance_id] = context

    return results


def instances(csv_file, participant_number):
    
    csv_reader = csv.reader(csv_file, delimiter=',')

    instance_data = None

    prev_instance_num = -2
    instance_num = -1

    cells_in_instance = []
    for instance_id, rows_of_instance in instance_rows(csv_reader):
        candidates = []

        for i, row in enumerate(rows_of_instance):

            cells = map(lambda x : x.replace(" ", "") , row[1:])
            is_numeric_line = i % 2 == 1
            
            if is_numeric_line:
                indexes = [i for i,x in enumerate(cells) if x == '1'] 
                
                for i, word in enumerate(candidate_words):
                    candidates.append(( word, 1 if i in indexes else 0 ))

            else:
                candidate_words = filter(lambda x: len(x) > 0, cells)

        instance_data = (instance_id, participant_number, candidates)
        yield instance_data

def participant_choices_from_csv(csv_filenames):
    """
    Returns (instance_id, participant_number, [ candidate translations ]) from 
    @param csv_filenames  the full path of the files
    """
    
    results = []
    for csv_filename in csv_filenames:
        with open(csv_filename, 'rb') as csv_file:
            for instance in instances(csv_file, csv_filename.split('/')[-1].split('.')[0][-2:]):
                results.append(instance)

    return results
       
def target_word(instance_id):
    return instance_id.split('#')[1].split("::")[0]

def oov_annotations(participant_annotations):
    instance_counter = defaultdict(list)

    oov_annotations_for = defaultdict(list)
    num_instances_with_oov_annotations = 0

    for participant_annotation in participant_annotations:
        instance_id     = participant_annotation[0]
        participant_num = participant_annotation[1]
        instance_counter[instance_id].append(participant_num)

    #    print "=== vvv"
        oov_annotations = out_of_vocab_annotations(( x.decode('utf-8') for x,y in participant_annotation[2] ), target_word(instance_id))
        if len(oov_annotations) > 0:
            oov_annotations_for[instance_id].extend(oov_annotations)
    #    for oov_annotation in oov_annotations:
    #        print oov_annotation.encode('utf-8')
    #    print "=== ^^^"

    #for key, value in instance_counter.iteritems():
    #    print key, value

    
    return oov_annotations_for

def selected_annotations(participant_annotations):
    results = defaultdict(lambda : defaultdict( int ))
    for participant_annotation in participant_annotations:
        instance_id     = participant_annotation[0]
        participant_num = participant_annotation[1]

        for candidate_word, count in participant_annotation[2]:
            #print candidate_word, count
            results[ instance_id ][ candidate_word ] += count

    return results

def filter_selected_annotations(participant_annotations, min_count = 1):
    selected_annotations_for = selected_annotations(participant_annotations)

    result = {}
    for instance_id, candidate_word_counts in selected_annotations_for.iteritems():
        result[instance_id] = []
        for candidate_translation, word_count in candidate_word_counts.iteritems():
            if word_count >= min_count:
                result[instance_id].append( candidate_translation )

    return result
    


if __name__ == "__main__":
    csvfiles = glob.glob(_config['directory'] + '*.csv')

    participant_annotations = participant_choices_from_csv(csvfiles)

    contexts_for        = contexts(csvfiles)
    oov_annotations_for = oov_annotations(participant_annotations)
    answers             = filter_selected_annotations(participant_annotations)
    answers_min_2       = filter_selected_annotations(participant_annotations, min_count=2)
    answers_min_3       = filter_selected_annotations(participant_annotations, min_count=3)

    participants_of_instance = defaultdict(set)
    for participant_annotation in participant_annotations:
        participant_num = participant_annotation[1]
        instance_id = participant_annotation[0]
        participants_of_instance[instance_id].add(participant_num)

 
    with codecs.open(_config['output_filename'], 'w+') as outfile,\
         codecs.open(_config['output_filename'] + "without_oov_instances", 'w+') as outfile_without_oov,\
         codecs.open(_config['output_filename'] + "with_minimum_2", 'w+') as outfile_min_2,\
         codecs.open(_config['output_filename'] + "with_minimum_3", 'w+') as outfile_min_3,\
         codecs.open(_config['output_filename'] + "without_answers", 'w+') as outfile_with_no_answers:


        ccc = 0
        for instance_id, participants in participants_of_instance.iteritems():
            
            if len(participants) > 3:
                print "---REQUIRES MANUAL PROCESSING HERE---"
                ccc+=1
                print instance_id, len(participants), participants
                print "---END REQUIRES MANUAL PROCESSING HERE--- ", ccc
                


        lets_include_oov_annotations = True
        

        ii = 0
        for instance_id in answers.keys():

            oov_annotation = [] if not lets_include_oov_annotations else oov_annotations_for[instance_id]
            oov_annotation_as_separated_string = '##' + '##'.join([annotation for annotation in oov_annotation ]) if len(oov_annotation) > 0 else ''

            selected_answers = answers[instance_id]
            answers_as_separated_string = '##'.join([selected_answer.decode('utf-8') for selected_answer in selected_answers])

            context = contexts_for[instance_id].replace('\n', ' ')

            is_part_of_np = is_target_word_proper_noun(  target_word(instance_id), context, instance_id )
            if is_part_of_np:
                print "is named entity "
                print context,'...', target_word(instance_id)
                

            assert len(target_word(instance_id)) > 0

            if len(selected_answers) + len(oov_annotation) == 0:
                outfile_with_no_answers.write(context + "   ## id : ## " + instance_id)
                outfile_with_no_answers.write("\n")
                continue
            if is_part_of_np and _config['remove_ne'] is True:
                continue

            try:
                selected_answer_min_2 = answers_min_2[instance_id]
                answer_as_separated_string_min_2 = '##'.join([selected_answer.decode('utf-8') for selected_answer in selected_answer_min_2])
            except KeyError:
                answer_as_separated_string_min_2 = ''

            try:
                selected_answer_min_3 = answers_min_3[instance_id]
                answer_as_separated_string_min_3 = '##'.join([selected_answer.decode('utf-8') for selected_answer in selected_answer_min_3])
            except KeyError:
                answer_as_separated_string_min_3 = ''



            sentence_with_head = ""
            has_added_target = False
            for token in context.split():
                if token.strip(string.punctuation).lower() == target_word(instance_id).lower() and not has_added_target:
                    sentence_with_head += target_word(instance_id).lower() + '##' + answers_as_separated_string + oov_annotation_as_separated_string + " "
                    has_added_target = True
                else:
                    sentence_with_head += token + " "

            sentence_with_head_min_2 = ""
            has_added_target = False
            for token in context.split():
                if token.strip(string.punctuation).lower() == target_word(instance_id).lower() and not has_added_target:
                    sentence_with_head_min_2 += target_word(instance_id).lower() + '##' + answer_as_separated_string_min_2 + " "
                    has_added_target = True
                else:
                    sentence_with_head_min_2 += token + " "

            sentence_with_head_min_3 = ""  
            has_added_target = False
            for token in context.split():
                if token.strip(string.punctuation).lower() == target_word(instance_id).lower() and not has_added_target:
                    sentence_with_head_min_3 += target_word(instance_id).lower() + '##' + answer_as_separated_string_min_3 + " "
                    has_added_target = True
                else:
                    sentence_with_head_min_3 += token + " "

            #sentence_with_head = re.sub('[^A-Za-z0-9]+' + target_word(instance_id) + '[^A-Za-z0-9]+', '\1 '+target_word(instance_id) + '##' + \
            #                            answers_as_separated_string + oov_annotation_as_separated_string +' \2', context, 1, re.IGNORECASE)

            outfile.write(sentence_with_head.encode('utf-8'))
            outfile.write('\n')

            if len(oov_annotation) == 0:
                outfile_without_oov.write(sentence_with_head.encode('utf-8'))
                outfile_without_oov.write('\n')

                #print(sentence_with_head_min_2)
                if len(selected_answer_min_2) > 0:
                    outfile_min_2.write(sentence_with_head_min_2.encode('utf-8'))
                    outfile_min_2.write('\n')
                if len(selected_answer_min_3) > 0:
                    outfile_min_3.write(sentence_with_head_min_3.encode('utf-8'))
                    outfile_min_3.write('\n')

            ii += 1


    print _pos_count
    print len(answers.keys())