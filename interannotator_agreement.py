"""
Inter annotator agreement...


confusion matrix:

                 A
            Top   Not-top 
B  Top              that doesn't match B's answer
 Not-top           has match

P(A) = top*top + non-top*non-top 

tMFS = propability MFS is chosen in the annotations

P(E) = tMFS^2 + (1-tMFS)^2



DITCHED because way too complicated and it actually sounds kind of stupid to be doing this!
"""


from mfs_answers import mfs_map
from produce_compiled_results_from_csv import participant_choices_from_csv

import glob

from collections import defaultdict

from IPython import embed


if __name__ == "__main__":
    mfs = mfs_map('/home/kanghj/Downloads/MUN_cleaned_data/MUN_cleaned_data/MFS.csv')

    participant_input_files = glob.glob('csv_format/*.csv')
    
    participant_annotations = participant_choices_from_csv(participant_input_files)
    # this contains (instnace_id, participant number, [answers ... ])

    # map by instance_id, then -> participant (1,2,3) (simply done by a string sort) 
    #                          -> answers
    instance_map_to_participant_to_answers = defaultdict(lambda : defaultdict(list))
    for instance_id, participant_id, answers in participant_annotations:
        participant_num = len(instance_map_to_participant_to_answers[instance_id])
        instance_map_to_participant_to_answers[instance_id][participant_num] = answers


    # confusions matrices are 4 x 4.  4 because 3 (top 3 senses) + 1 (outside of top 3)
    confusion_matrix_12 = [ [0, 0, 0, 0],\
                            [0, 0, 0, 0],\
                            [0, 0, 0, 0],\
                            [0, 0, 0, 0]]
    confusion_matrix_13 = [ [0, 0, 0, 0],\
                            [0, 0, 0, 0],\
                            [0, 0, 0, 0],\
                            [0, 0, 0, 0]]
    confusion_matrix_23 = [ [0, 0, 0, 0],\
                            [0, 0, 0, 0],\
                            [0, 0, 0, 0],\
                            [0, 0, 0, 0]]

    # no match are the number of times the out of top 5 annotations dont match at all
    no_match_12 = 0
    no_match_13 = 0
    no_match_23 = 0

    times_participant_mfs = [[], [], [], [], []]
    times_participant_mfs[0] = [0, 0 , 0]  #times_participant_mfs[0][2] is num times participant 2 annotated top most freqent sense
    times_participant_mfs[1] = [0, 0 , 0]
    times_participant_mfs[2] = [0, 0 , 0]



    for instance_id in instance_map_to_participant_to_answers.keys():

        # for this instance

        # annotator_answers is [ [zh_word ...] , [zh_word ...], [zh_word ...]]
        annotator_answers = []
        annotator_answers.append(\
                [candidate_translation \
                    for candidate_translation, is_annotated in instance_map_to_participant_to_answers[instance_id][0]\
                                        if is_annotated])
        annotator_answers.append(\
                [candidate_translation \
                    for candidate_translation, is_annotated in instance_map_to_participant_to_answers[instance_id][1]\
                                        if is_annotated])
        annotator_answers.append(\
                [candidate_translation \
                    for candidate_translation, is_annotated in instance_map_to_participant_to_answers[instance_id][2]\
                                        if is_annotated])
        # annotator_answers is [ [zh_word ...] , [zh_word ...], [zh_word ...]]

        target_word = instance_id.split('#')[1].split('::')[0]

        try:
            most_frequent_answer = []
            most_frequent_answer.append(mfs[target_word][0])
            most_frequent_answer.append(mfs[target_word][1])
            most_frequent_answer.append(mfs[target_word][2])
        except:
            for i in range(3 - len(most_frequent_answer)):
                most_frequent_answer.append('')
            print 'not enough mfs', target_word


        annotator_answered_mfs      = map(lambda x : most_frequent_answer[0] in x, annotator_answers)
        annotator_answered_mfs_1    = map(lambda x : most_frequent_answer[1] in x, annotator_answers)
        annotator_answered_mfs_2    = map(lambda x : most_frequent_answer[2] in x, annotator_answers)

        # annotator_answered_mfs is [True, False, True]   # True is annotator[i] contains the mfs
        # annotator_answered_mfs_1 is [True, False, True] # True is annotator[i] contains the second mfs

        
        has_non_mfs_match = [[False, False, False], [False, False, False], [False, False, False]]
        for participant, participant_answers in enumerate(annotator_answers):
            for inner_participant, inner_participant_answers in enumerate(annotator_answers):
                if participant == inner_participant:
                    continue
                for answer in participant_answers:
                    if answer in most_frequent_answer:
                        continue

                    if answer in inner_participant_answers:
                        has_non_mfs_match[participant][inner_participant] = True
                        has_non_mfs_match[inner_participant][participant] = True
        



        times_participant_mfs[0][0] += annotator_answered_mfs[0]
        times_participant_mfs[0][1] += annotator_answered_mfs[1]
        times_participant_mfs[0][2] += annotator_answered_mfs[2]

        times_participant_mfs[1][0] += annotator_answered_mfs_1[0]
        times_participant_mfs[1][1] += annotator_answered_mfs_1[1]
        times_participant_mfs[1][2] += annotator_answered_mfs_1[2]

        times_participant_mfs[2][0] += annotator_answered_mfs_2[0]
        times_participant_mfs[2][1] += annotator_answered_mfs_2[1]
        times_participant_mfs[2][2] += annotator_answered_mfs_2[2]


        
        if annotator_answered_mfs[0] and annotator_answered_mfs[1]:
            confusion_matrix_12[0][0] += 1
        else:
            
            if has_non_mfs_match[0][1]:
                confusion_matrix_12[3][3] += 1
            elif annotator_answered_mfs[0]:

                confusion_matrix_12[0][1] += 1
            elif annotator_answered_mfs[1]:
                confusion_matrix_12[1][0] += 1
            else:
                no_match_12 += 1
    
        """
        if annotator_answered_mfs[0] and annotator_answered_mfs[2]:
            confusion_matrix_13[0][0] += 1
        else:
            
            if has_non_mfs_match[0][2]:
                confusion_matrix_13[1][1] += 1 
            elif annotator_answered_mfs[0]:
                confusion_matrix_13[0][1] += 1
            elif annotator_answered_mfs[2]:
                confusion_matrix_13[1][0] += 1
            else:

                no_match_13 += 1


        if annotator_answered_mfs[1] and annotator_answered_mfs[2]:
            confusion_matrix_23[0][0] += 1
        else:
            
            if has_non_mfs_match[1][2]:
                confusion_matrix_23[1][1] += 1
            elif annotator_answered_mfs[1]:
                confusion_matrix_23[0][1] += 1
            elif annotator_answered_mfs[2]:
                confusion_matrix_23[1][0] += 1
            else:
              
                no_match_23 += 1

    print "Confusion Matrix for first and second participant"
    for values in confusion_matrix_12:
        for value in values:
            print value, ",",
        print ""
    print no_match_12

    print '==='
    print "Confusion Matrix for first and third participant"
    for values in confusion_matrix_13:
        for value in values:
            print value, ",",
        print ""
    print no_match_13

    print '==='
    print "Confusion Matrix for second and third participant"
    for values in confusion_matrix_23:
        for value in values:
            print value, ",",
        print ""
    print no_match_23
    """

    print '==='
    print "Times each participant annotated mfs : "
    print times_participant_mfs[0]
    print times_participant_mfs[1]
    print times_participant_mfs[2]
    print times_participant_mfs[3]
    print times_participant_mfs[4]
