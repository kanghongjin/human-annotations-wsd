"""
From ..., produce a file of 
lemma lemma_id sense_id

"""
import os
import sys
from collections import defaultdict

from mstranslator import mstranslator

import convert_to_ims_format


client_id = os.environ['TEST_MSTRANSLATOR_CLIENT_ID']
client_secret = os.environ['TEST_MSTRANSLATOR_CLIENT_SECRET']


translator = mstranslator.Translator(client_id, client_secret)

_config = {'output_directory' : 'bing_answers'}

def index_of_target_word_in_line(line, target_word):
    return line.index('#') - len(target_word)

def is_in_alignment_range(alignment, target_word_index):
    start = int(alignment.split('-')[0].split(':')[0])
    end = int(alignment.split('-')[0].split(':')[1])


    return start <= target_word_index < end

def is_in_strict_alignment_range(alignment, target_word_index, target_word):
    start = int(alignment.split('-')[0].split(':')[0])
    end = int(alignment.split('-')[0].split(':')[1])

    if not (start <= target_word_index < end):
        return False

    return end - start + 1 == len(target_word)

def alignment_end(alignment, translation):
    start = int(alignment.split('-')[1].split(':')[0])
    end = int(alignment.split('-')[1].split(':')[1]) + 1

    return translation[start:end]


if __name__ == "__main__":
    out_directory = _config['output_directory']
    if not os.path.exists(out_directory):
        os.makedirs(out_directory)

    lexelt_counts = defaultdict(lambda : 0)

    with open('compiled_all.txtwith_minimum_3', 'r') as all_words_file,\
         open('bing_answer_min_3.txt', 'w+') as result_file,\
         open('bing_answer_strict_min_3.txt', 'w+') as strict_result_file:
        for line in all_words_file:
            reconstruct_tokens = []
            for token in line.split():
                if '#' not in token:
                    reconstruct_tokens.append(token)
                    continue
                target_word = token.split('#')[0]
                reconstruct_tokens.append(target_word)

            index_of_target_word = index_of_target_word_in_line(line, target_word)
            if index_of_target_word < 0:
                raise ValueError('index not found!')

            line = ' '.join(reconstruct_tokens)

            ts = translator.translate_array2([line], 'en' , 'zh')

            if len(ts) != 1:
                raise ValueError('no translations?, or too many')

            t = ts[0]
            translations =  t['TranslatedText']
            alignments   =  t['Alignment'] 

            bing_answer = 'U'
            strict_bing_answer = 'U'
            for alignment in alignments.split(' '):
                
                if is_in_alignment_range(alignment, index_of_target_word):
                    #sys.stderr.write( 'inside!' + alignment + str(index_of_target_word) +'\n' )

                    bing_answer = alignment_end(alignment, translations)


                    if is_in_strict_alignment_range(alignment, index_of_target_word, target_word):

                        strict_bing_answer = alignment_end(alignment, translations)


            if bing_answer != 'U':
                try:
                    bing_answer = convert_to_ims_format.get_senseid(bing_answer.encode('utf-8'))
                except KeyError:
                    bing_answer = 'OOV ' + bing_answer
            if strict_bing_answer != 'U':
                try:
                    strict_bing_answer = convert_to_ims_format.get_senseid(strict_bing_answer.encode('utf-8'))
                except KeyError:
                    strict_bing_answer = 'OOV ' + strict_bing_answer

            instance_id = target_word + '.' + str(lexelt_counts[target_word])
            result_file.write( "docsrc " + instance_id + " " + str(bing_answer)  + '\n')
            strict_result_file.write( "docsrc " + instance_id + " " + str(strict_bing_answer) + '\n')
            lexelt_counts[target_word] += 1