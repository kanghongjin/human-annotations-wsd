"""
For the instances in compiled_all.txtwithout_oov_instances,
runs a pos tagger over them,

produces a mapping of id->pos tag,
groups the answers in bing_answers_strict, context_sum, ims


the files () bing_answers_strict, context_sum, ims ) in analysis_at_word_level are generated copying-pasting from ims
CScorer /scorer.bash output
"""

import sys

import nltk
from collections import defaultdict, Counter

import glob


from word_level_analysis import get_word_level_accuracy

_config = {
    'compiled_instances_file' : 'compiled_all.txtwithout_oov_instances'
}

def index_of_target_word_in_line(line, target_word):
    return line.index('#') - len(target_word)

def get_pos_level_accuracy(filename, map_of_instance_id_to_pos):
    lemma_correct_answers = Counter()
    lemma_total = Counter()

    pos_correct_answers = Counter()
    pos_total = Counter()

    with open(filename, 'r') as answer_file:
        for line in answer_file:
            tokens = line.split(',')
            lemma = tokens[0].split('_')[0].strip()            

            is_correct = tokens[1].strip() == "correct"

            lemma_total[lemma] += 1 
            if is_correct:
                lemma_correct_answers[lemma] += 1

            instance_id = tokens[0].split('_')[1].strip() # get something like 'real.1'
            pos_of_instance = map_of_instance_id_to_pos[instance_id]

            if is_correct:
                pos_correct_answers[pos_of_instance] += 1
            pos_total[pos_of_instance] += 1
    
    pos_to_accuracy = {}
    for pos in pos_total.keys():
        total_cases = pos_total[pos]

        sys.stderr.write( pos + " : " + str(total_cases) + '\n' )
        pos_to_accuracy[pos] = float(pos_correct_answers[pos]) / total_cases

    

    return pos_to_accuracy

    
    

if __name__ == "__main__":

    lexelt_counts = defaultdict(lambda : 0)

    map_of_instance_id_to_pos = {}

    CONFLATE_VERBS = True
    with open('compiled_all.txtwithout_oov_instances', 'r') as instances_file:
         
        for line in instances_file:
            reconstruct_tokens = []

            for i, token in enumerate(line.split()):
                if '#' not in token:
                    reconstruct_tokens.append(token)
                    continue
                target_word = token.split('#')[0]
                reconstruct_tokens.append(target_word)
            

            index_of_target_word = index_of_target_word_in_line(line, target_word)
            if index_of_target_word < 0:
                raise ValueError('index not found!')

            line = ' '.join(reconstruct_tokens)

            
            tokens_with_pos = nltk.pos_tag(\
                                            nltk.tokenize.word_tokenize(line)\
            )

            for token_with_pos in tokens_with_pos:
                if token_with_pos[0] == target_word:
                    target_word_token_with_pos = token_with_pos
            print target_word
            print target_word_token_with_pos

            instance_id = target_word + '.' + str(lexelt_counts[target_word])
            lexelt_counts[target_word] += 1



            if not CONFLATE_VERBS:
                pos = target_word_token_with_pos[1]
            else:
                if target_word_token_with_pos[1].startswith('VB'):
                    pos = 'VB'
                else:
                    pos = target_word_token_with_pos[1]


            map_of_instance_id_to_pos[instance_id] = pos


    print map_of_instance_id_to_pos
          

    filenames = glob.glob('./analysis_at_word_level/*.csv')

    pos_accuracy = defaultdict(list)

    print " , ",
    for filename in filenames:
        
        pos_level_accuracy = get_pos_level_accuracy(filename, map_of_instance_id_to_pos)

        print filename.split('/')[-1] + ", ", # csv header
        

        for lemma, accuracy in pos_level_accuracy.iteritems():
            pos_accuracy[lemma].append(accuracy)

    print ""

    for lemma, answers in pos_accuracy.iteritems():
        print lemma, ",", 
        for answer in answers:
            print answer, ",",
        
        print ""
       