import os
import itertools
from collections import defaultdict, Counter
import glob
import csv
import sqlite3
import codecs
import re
import string
from IPython import embed
import nltk

"""


"""

context     = {}
target_word = {}

_config = {"directory" : "./csv_format/"
           }

conn = sqlite3.connect(_config['dictionary_sqlite'])
c = conn.cursor()


_INSTANCE_ID_COL_NUM = 6

_pos_count = defaultdict(list)



def instance_rows(csv_reader):
    """

    """
    instance = []

    times_first_cell_has_value = 0
    prev_returned_line_number = 0

    for i, row in enumerate(csv_reader):
        if row[0].strip() != "":
            times_first_cell_has_value += 1
            
        if times_first_cell_has_value % 2 != 0:

            if abs(i - prev_returned_line_number) > 1:
                yield (instance_id, instance)
                instance = []

            instance_id = row[_INSTANCE_ID_COL_NUM]

        else:
            instance.append(row)

def contexts(csv_filenames):
    results = {}
    for csv_filename in csv_filenames:
        with open(csv_filename, 'rb') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for instance_id, rows_of_instance in instance_rows(csv_reader):
                context = rows_of_instance[0][0]
                results[instance_id] = context

    return results


def instances(csv_file, participant_number):
    
    csv_reader = csv.reader(csv_file, delimiter=',')

    instance_data = None

    prev_instance_num = -2
    instance_num = -1

    cells_in_instance = []
    for instance_id, rows_of_instance in instance_rows(csv_reader):
        candidates = []

        for i, row in enumerate(rows_of_instance):

            cells = map(lambda x : x.replace(" ", "") , row[1:])
            is_numeric_line = i % 2 == 1
            
            if is_numeric_line:
                indexes = [i for i,x in enumerate(cells) if x == '1'] 
                
                for i, word in enumerate(candidate_words):
                    candidates.append(( word, 1 if i in indexes else 0 ))

            else:
                candidate_words = filter(lambda x: len(x) > 0, cells)

        instance_data = (instance_id, participant_number, candidates)
        yield instance_data

def participant_choices_from_csv(csv_filenames):
    """
    Returns (instance_id, participant_number, [ candidate translations ]) from 
    @param csv_filenames  the full path of the files
    """
    
    results = []
    for csv_filename in csv_filenames:
        with open(csv_filename, 'rb') as csv_file:
            for instance in instances(csv_file, csv_filename.split('/')[-1].split('.')[0][-2:]):
                results.append(instance)

    return results
       
def target_word(instance_id):
    return instance_id.split('#')[1].split("::")[0]

def oov_annotations(participant_annotations):
    instance_counter = defaultdict(list)

    oov_annotations_for = defaultdict(list)
    num_instances_with_oov_annotations = 0

    for participant_annotation in participant_annotations:
        instance_id     = participant_annotation[0]
        participant_num = participant_annotation[1]
        instance_counter[instance_id].append(participant_num)

    #    print "=== vvv"
        oov_annotations = out_of_vocab_annotations(( x.decode('utf-8') for x,y in participant_annotation[2] ), target_word(instance_id))
        if len(oov_annotations) > 0:
            oov_annotations_for[instance_id].extend(oov_annotations)
    #    for oov_annotation in oov_annotations:
    #        print oov_annotation.encode('utf-8')
    #    print "=== ^^^"

    #for key, value in instance_counter.iteritems():
    #    print key, value

    
    return oov_annotations_for

def selected_annotations(participant_annotations):
    results = defaultdict(lambda : defaultdict( int ))
    for participant_annotation in participant_annotations:
        instance_id     = participant_annotation[0]
        participant_num = participant_annotation[1]

        for candidate_word, count in participant_annotation[2]:
            #print candidate_word, count
            results[ instance_id ][ candidate_word ] += count

    return results


if __name__ == "__main__":
    csvfiles = glob.glob(_config['directory'] + '*.csv')

    participant_annotations = participant_choices_from_csv(csvfiles)

    contexts_for        = contexts(csvfiles)
    oov_annotations_for = oov_annotations(participant_annotations)
    answers             = filter_selected_annotations(participant_annotations)

    participants_of_instance = defaultdict(set)
    for participant_annotation in participant_annotations:
        participant_num = participant_annotation[1]
        instance_id = participant_annotation[0]
        participants_of_instance[instance_id].add(participant_num)

 
