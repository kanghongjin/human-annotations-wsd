import glob

_config = {'inputs_filename': 'compiled_all.txt'}



if __name__ == "__main__":
    filenames = glob.glob('./' + _config['inputs_filename'] + '*')
    print filenames
    
    for filename in filenames:

        unique_target_words = set()

        with open(filename, 'r') as input_file:
            for line in input_file:

                for token in line.split():
                    if '#' not in token:
                        continue
                    target_word = token.split('#')[0]
                    unique_target_words.add(target_word)
        print filename, unique_target_words


    
