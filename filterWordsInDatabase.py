import sqlite3
import itertools
import re
import random
from IPython import embed

config = {
            # only supports sqlite3 for now
            'database': 'dictionary_plus_naijia_ground_truth.db',
            'english_word_table': 'chinese_words',
            'chinese_word_table': 'english_words',
            'english_word_col': 'english_meaning',
            'chinese_word_col': 'chinese_meaning',
            'joined_table' : 'meanings',
            'english_file': 'tokenized_sentences.en', # file for sentence-aligned english
            'chinese_file': 'segmented_sentences.zh', # file for sentence-aligned chinese

            'output_english': 'en.txt',
            'output_chinese': 'ch.txt',
            'output_swapped': 'swapped_training.txt',

            'word_alignment_file': '115-08-27.232031.kanghj.A3.final',
         }


print(config)
conn = sqlite3.connect(config['database'])
c = conn.cursor()

######
# The goal is to find sentences in which both the english_word appear
# in the english sentence, and a chinese_word linked to the english_word
# appears in the corresponding chinese sentence.
######

def get_chinese_words_for_english(english_word):
    c.execute("\
                SELECT chinese_meaning FROM english_words \
                INNER JOIN meanings \
                ON english_words.id = meanings.english_word_id \
                INNER JOIN chinese_words \
                ON meanings.chinese_word_id = chinese_words.id \
                WHERE english_meaning = ? \
              ", (english_word, ))
    result = c.fetchall()

    return result

def print_chinese(word):
    print word.encode('utf-8')

count_does_not_agree = 0
count_agree = 0

def get_relevant_word_pairs(english_line, chinese_line, english_word, word_alignment_info = None):
    """
        return list of (english_word, chinese_word) if
        the english_word is found in english_line
        and chinese_word is found in chinese_line
    """
    global count_agree
    global count_does_not_agree

    results = []
    # get paired chinese words from db
    possible_chinese_words = construct_set_of_chinese_words(english_word)

    for chinese_word in possible_chinese_words:
        if word_alignment_info is None:
            # simply include the word pair if the chinese word appears somewhere in the line
            if chinese_word.encode('utf-8') in chinese_line:
                results.append((english_word, chinese_word))
        else:
            # include the word pair only if the word_alignment_info agrees
            chinese_line_as_list = chinese_line.split()
            try:
                for possible_position in word_alignment_info[english_word]:
                    # word alignment file is 1-indexed
                    possible_position -= 1
                    ##print english_word, possible_position
                    if chinese_line_as_list[possible_position].decode('utf-8') == chinese_word:
                        results.append((english_word, chinese_word))
                        count_agree += 1
                        break
                    else:
                        count_does_not_agree += 1
                        print "does not agree", count_does_not_agree
                        print '     vs agreeable: ', count_agree
            except KeyError:
                print '====='
                print english_word
                print english_line
                for hmm in chinese_line_as_list:
                    print hmm,
                print word_alignment_info
                print "key erorr!!! ^^^^"

                continue
            except IndexError as e:
                print '------'
                print e
                print english_word
                print english_line
                for hmm in chinese_line_as_list:
                    print hmm,
                print word_alignment_info
                print '------'
                break


    return results


def get_all_english_words_in_dictionary():
    c.execute("SELECT english_meaning FROM english_words")
    result = c.fetchall()

    return result

def construct_set_of_chinese_words(english_word):
    possible_chinese_words = get_chinese_words_for_english(english_word)
    return construct_set_from_database_result(possible_chinese_words)


def construct_set_of_english_words():
    list_of_english_word_tuples = get_all_english_words_in_dictionary()
    return construct_set_from_database_result(list_of_english_word_tuples)

def construct_set_from_database_result(db_result):
    result_as_list = map(lambda x: x[0], db_result)
    return frozenset(result_as_list)

def construct_swapped_sentences(word_pair, english_line, chinese_line):
    """
    Given word pair (eng_word, chi_word), swaps the occurence of
    chi_word with eng_word in chinese_line,
    and swaps the *first* occurence of eng_word with chi_word in english_line.
    Returns a tuple (processed_english_line, processed_chinese_line)
    """
    processed_english_line = english_line.replace(' ' + word_pair[0] + ' ', ' ' + word_pair[0] + '##' + word_pair[1].encode('utf-8') + ' ', 1)
   # processed_chinese_line = chinese_line.replace(word_pair[1].encode('utf-8'), word_pair[0], 1) # no need swapped chinese line now
    #processed_english_line = re.sub(r"\b%s\b" % (word_pair[0],), word_pair[1].encode('utf-8'), english_line)
    #processed_chinese_line = chinese_line.replace(word_pair[1].encode('utf-8'), word_pair[0])

    return (processed_english_line, '')



def build_training_data(english_output, chinese_output, swapped_output, word_alignment_info = None):
    english_words = construct_set_of_english_words()

    with open(config['english_file'], 'r') as ef, open(config['chinese_file'], 'r') as cf:
        for i, (english_line, chinese_line) in enumerate(itertools.izip(ef, cf)):

            contains_word_pair = False
            for token in set(english_line.split()):
                if token not in english_words:
                    continue
                word_pairs = get_relevant_word_pairs(english_line, chinese_line, token, word_alignment_info[int(i)] if word_alignment_info is not None else None)
                if len(word_pairs) == 0:
                    continue

                contains_word_pair = True

                """for word_pair in word_pairs:
                    swapped_english_line, swapped_chinese_line \
                        = construct_swapped_sentences(word_pair, english_line, chinese_line)
                    swapped_output.write(swapped_english_line)
                    swapped_output.write(swapped_chinese_line)
                """  # let's only use the first pair
                #word_pair = word_pairs[0]
                #swapped_english_line, swapped_chinese_line \
                #    = construct_swapped_sentences(word_pair, english_line, chinese_line)
                #swapped_output.write(swapped_english_line)
                #swapped_output.write(swapped_chinese_line) # i think there is no need to have chinese word context


            if contains_word_pair:
                english_output.write(english_line)
                chinese_output.write(chinese_line)
            if i % 1000 == 0:
                print i



def construct_structure_for_word_alignment(alignment_file):
    possible_chinese_words = {}
    for i, line in enumerate(alignment_file):
        if i%3 == 0:
            possible_chinese_words_for_line = {}
            possible_chinese_words[int(i/3)] = possible_chinese_words_for_line
            continue
        elif i%3 == 1:
            # target language line
            target_line = line.split()
        else:
            # source language line
            has_closed = True
            for j, word in enumerate(line.split()):
                if has_closed and not is_int(word) and ('{' not in word and '}' not in word): ##### fix
                    english_word = word
                    possible_chinese_words_for_line[english_word] = []
                elif not has_closed and is_int(word):
                    possible_chinese_words_for_line[english_word].append(int(word))
                elif '{' in word:
                    has_closed = False
                elif '}' in word:
                    has_closed = True
    return possible_chinese_words


def is_int(word):
    try:
        int(word)
        return True
    except:
        return False









## todo yada yada _______main___ blah blah blah

with open(config['output_english'], 'w') as english_output, \
     open(config['output_chinese'], 'w') as chinese_output, \
     open(config['output_swapped'], 'w') as swapped_output, \
     open(config['output_swapped'] + '_test', 'w') as test_output:
#     open(config['word_alignment_file']) as word_alignment_file:

    #possible_chinese_words = construct_structure_for_word_alignment(word_alignment_file)

    build_training_data(english_output, chinese_output, swapped_output)

    #build_test_data(test_output, possible_chinese_words)

