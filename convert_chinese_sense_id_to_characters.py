import sqlite3
import itertools
import re
import random

import convert_to_ims_format

conn = sqlite3.connect('dictionary_plus_naijia_ground_truth.db')
c = conn.cursor()

def get_chinese_word(senseid):
    # hard code a fix! TODO a better fix

    c.execute("SELECT chinese_meaning FROM chinese_words WHERE id = ?", (senseid,))
    result = c.fetchall()

    if len(result) == 0:
        raise KeyError()
    
    try:
        result =  get_val_from_database_result(result)
        
    except Exception as e:
        raise ValueError("failed to get chinese word ", e)
    return result.replace(' ... ', '...')

def get_chinese_words_for_list(senseids):
    """
    TODO: merge with get_senseid. duck typing yada yada. optimise as a single query blah blah blah
    """
    result = []
    for senseid in senseids:
        result.append(get_chinese_word(senseid))

    return result

def get_val_from_database_result(db_result):
    result_as_list = map(lambda x: x[0], db_result)
    return result_as_list[0]

    