import sqlite3
import xml.etree.cElementTree as ET
import codecs
import string

# write to key file and xml file

#xml has lexelt item="lemma.pos"
#        instance (id, docsrc)
#        answer(instance, senseid)
#        context

conn = sqlite3.connect('dictionary_plus_naijia_ground_truth.db')
c = conn.cursor()

def getlexelts():
    # get english words from database.db
    c.execute("SELECT english_meaning FROM english_words")
    result = c.fetchall()

    return construct_set_from_database_result(result)

def construct_set_from_database_result(db_result):
    result_as_list = map(lambda x: x[0], db_result)
    return frozenset(result_as_list)
    # return as a set

def get_senseid(chinese_word):
    # hard code a fix! TODO a better fix
    if '...' in chinese_word:
        chinese_word = chinese_word.replace('...', ' ... ')
    print chinese_word
    c.execute("SELECT id FROM chinese_words WHERE chinese_meaning = ?", (chinese_word.decode('utf-8'),))

    result = c.fetchall()
    try:
        return get_val_from_database_result(result)
    except:
        return -1

def get_senseid_for_list(chinese_words):
    """
    TODO: merge with get_senseid. duck typing yada yada. optimise as a single query blah blah blah
    """
    result = []
    for chinese_word in chinese_words:
        result.append(get_senseid(chinese_word))

    return result

def get_val_from_database_result(db_result):
    result_as_list = map(lambda x: x[0], db_result)
    return result_as_list[0]


# key is lemma instanceid senseid

config = {
        'training_data' : 'compiled_all.txtwithout_oov_instances',
        }

instance_to_context_map = {}
instance_answer = {}

instance_count = {}

if __name__ == '__main__':
    with open(config['training_data'], 'r') as en_f:
        line_counter = 0
        for line in en_f:
            line_counter+=1

            tokenised_sent = line.strip().split(" ")

            target_word = None
            target_token = None
            instance_id = None
            answer = None

            for token in tokenised_sent:

                if '##' in token:
                    target_token = token
                    target_word = token.split('##')[0]
                    answer = [symbol.rstrip(string.punctuation) for symbol in token.split('##')[1:]]

                    try:
                        instance_id = instance_count[target_word]
                        instance_count[target_word] += 1
                    except:
                        instance_id = 0
                        instance_count[target_word] = 1

            if target_word is None:
                print 'err', tokenised_sent, line_counter
                continue # ignore and cont

            instance = target_word + '.' + str(instance_id)

            if len(answer) == 1:
                instance_answer[instance] = [ get_senseid( answer[0] ) ]
            else:
                instance_answer[instance] = get_senseid_for_list( answer )
            instance_to_context_map[instance] = line.replace(target_token, '___head:___' + target_word + '___:head___')


    keyfile = codecs.open('./generated_ims_format_training.key', 'w', 'utf-8')
    root = ET.Element("corpus")
    root.set('lang', 'english')

    # iter over lexelt
    lexelts = getlexelts()
    for lexelt in lexelts:
        if lexelt not in instance_count :
            continue # skip if no occurence of lexelt
        lexelt_node = ET.SubElement(root, "lexelt")
        lexelt_node.set('item', lexelt)



        # add children
        for instanceId in xrange(0, instance_count[lexelt]):
            instance_node = ET.SubElement(lexelt_node, 'instance')
            formatted_instance_id = lexelt + '.' + str(instanceId)
            instance_node.set('id', formatted_instance_id)
            instance_node.set('docsrc', 'wordnews')

            
            #    answer_node = ET.SubElement(instance_node, 'answer')
            #    answer_node.set('instance', formatted_instance_id)
            #    answer_node.set('senseid', ' '.join([ str(ans) for ans in instance_answer[formatted_instance_id]]))

            keyfile.write(lexelt + ' ' + formatted_instance_id + ' ' + ' '.join([ str(ans) for ans in instance_answer[formatted_instance_id]]) + '\n')

            context_node = ET.SubElement(instance_node, 'context')
            context_node.text = instance_to_context_map[formatted_instance_id].decode('utf8')

    tree = ET.ElementTree(root)
    print tree
    tree.write('./generated_ims_format_temp.xml', encoding="UTF-8",xml_declaration=True)

    keyfile.close()

    with codecs.open('./generated_ims_format_temp.xml', 'r', 'utf-8') as infile:
        outfile_name = './generated_ims_format_training.xml' 
        outfile = codecs.open(outfile_name, 'w', 'utf-8')
        for line in infile:
            mod_line = line.replace('___head:___', '<head>')
            mod_line = mod_line.replace('___:head___', '</head>')
            outfile.write(mod_line)
        outfile.close()
