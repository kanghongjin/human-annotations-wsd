"""
Inter annotator agreement...


confusion matrix:

                 A
            Top   Not-top 
B  Top              that doesn't match B's answer
 Not-top           has match

P(A) = top*top + non-top*non-top 

tMFS = propability MFS is chosen in the annotations

P(E) = tMFS^2 + (1-tMFS)^2

"""


from mfs_answers import mfs_map
from produce_compiled_results_from_csv import participant_choices_from_csv

import glob

from collections import defaultdict

from IPython import embed


if __name__ == "__main__":
    mfs = mfs_map('/home/kanghj/Downloads/MUN_cleaned_data/MUN_cleaned_data/MFS.csv')

    participant_input_files = glob.glob('csv_format/*.csv')
    
    participant_annotations = participant_choices_from_csv(participant_input_files)
    # this contains (instnace_id, participant number, [answers ... ])

    # map by instance_id, then -> participant (1,2,3) (simply done by a string sort) 
    #                          -> answers
    instance_map_to_participant_to_answers = defaultdict(lambda : defaultdict(list))
    for instance_id, participant_id, answers in participant_annotations:
        participant_num = len(instance_map_to_participant_to_answers[instance_id])
        instance_map_to_participant_to_answers[instance_id][participant_num] = answers



    match_12 = 0
    match_13 = 0
    match_23 = 0

    times_participant_mfs = [[], [], [], [], []]
    times_participant_mfs[0] = [0, 0 , 0]  #times_participant_mfs[0][2] is num times participant 2 annotated top most freqent sense
    times_participant_mfs[1] = [0, 0 , 0]
    times_participant_mfs[2] = [0, 0 , 0]



    for instance_id in instance_map_to_participant_to_answers.keys():

        # for this instance

        # annotator_answers is [ [zh_word ...] , [zh_word ...], [zh_word ...]]
        annotator_answers = []
        annotator_answers.append(\
                [candidate_translation \
                    for candidate_translation, is_annotated in instance_map_to_participant_to_answers[instance_id][0]\
                                        if is_annotated])
        annotator_answers.append(\
                [candidate_translation \
                    for candidate_translation, is_annotated in instance_map_to_participant_to_answers[instance_id][1]\
                                        if is_annotated])
        annotator_answers.append(\
                [candidate_translation \
                    for candidate_translation, is_annotated in instance_map_to_participant_to_answers[instance_id][2]\
                                        if is_annotated])
        # annotator_answers is [ [zh_word ...] , [zh_word ...], [zh_word ...]]

        target_word = instance_id.split('#')[1].split('::')[0]

        try:
            most_frequent_answer = []
            most_frequent_answer.append(mfs[target_word][0])
            most_frequent_answer.append(mfs[target_word][1])
            most_frequent_answer.append(mfs[target_word][2])
        except:
            for i in range(3 - len(most_frequent_answer)):
                most_frequent_answer.append('')
            print 'not enough mfs', target_word


        annotator_answered_mfs      = map(lambda x : most_frequent_answer[0] in x, annotator_answers)
        annotator_answered_mfs_1    = map(lambda x : most_frequent_answer[1] in x, annotator_answers)
        annotator_answered_mfs_2    = map(lambda x : most_frequent_answer[2] in x, annotator_answers)

        # annotator_answered_mfs is [True, False, True]   # True is annotator[i] contains the mfs
        # annotator_answered_mfs_1 is [True, False, True] # True is annotator[i] contains the second mfs

        
        match_12 += len(set(annotator_answers[0]).intersection(annotator_answers[1])) > 0
        match_13 += len(set(annotator_answers[0]).intersection(annotator_answers[2])) > 0
        match_23 += len(set(annotator_answers[1]).intersection(annotator_answers[2])) > 0



        times_participant_mfs[0][0] += annotator_answered_mfs[0]
        times_participant_mfs[0][1] += annotator_answered_mfs[1]
        times_participant_mfs[0][2] += annotator_answered_mfs[2]

        times_participant_mfs[1][0] += annotator_answered_mfs_1[0]
        times_participant_mfs[1][1] += annotator_answered_mfs_1[1]
        times_participant_mfs[1][2] += annotator_answered_mfs_1[2]

        times_participant_mfs[2][0] += annotator_answered_mfs_2[0]
        times_participant_mfs[2][1] += annotator_answered_mfs_2[1]
        times_participant_mfs[2][2] += annotator_answered_mfs_2[2]


     


    num_annotations = float(len(instance_map_to_participant_to_answers.keys()))

    print '==='
    print "Times each participant annotated mfs : "
    print times_participant_mfs[0], "  : ", 
    print times_participant_mfs[0][0] / num_annotations,
    print times_participant_mfs[0][1] / num_annotations,
    print times_participant_mfs[0][2] / num_annotations,
    print ""

    print times_participant_mfs[1], "  : ",
    print times_participant_mfs[1][0] / num_annotations,
    print times_participant_mfs[1][1] / num_annotations,
    print times_participant_mfs[1][2] / num_annotations,
    print ""

    print times_participant_mfs[2], "  : ",
    print times_participant_mfs[2][0] / num_annotations,
    print times_participant_mfs[2][1] / num_annotations,
    print times_participant_mfs[2][2] / num_annotations,
    print ""

    
    print '==='
    print "Times match : "
    print "1,2", match_12    
    print "1,3", match_13
    print "2,3", match_23

    print '==='
    print "Times no match : "
    
    print "1,2", num_annotations - match_12    
    print "1,3", num_annotations - match_13
    print "2,3", num_annotations - match_23