
from collections import defaultdict

def mfs_map(location):
    mfs = {}
    with open(location, 'r') as opened:
        opened.readline() # consume first line consisting of header
        for line in opened:
            tokens = line.split(',')
            lemma = tokens[0].strip()
            zh_word = tokens[1].strip()

            zh_word_2 = tokens[4].strip()
            zh_word_3 = tokens[7].strip()

            if lemma in mfs:
                raise ValueError()
            mfs[lemma] = (zh_word, zh_word_2, zh_word_3)
    return mfs

def mfs_senseid_map(location):
    mfs = {}
    with open(location, 'r') as opened:
        opened.readline() # consume first line consisting of header
        for line in opened:
            tokens = line.split(',')
            lemma = tokens[0].strip()
            senseid = tokens[3].strip()

            if lemma in mfs:
                raise ValueError()
            mfs[lemma] = senseid
    return mfs

if __name__ == "__main__":
    
    mfs = mfs_senseid_map('/home/kanghj/Downloads/MUN_cleaned_data/MUN_cleaned_data/MFS.csv')

    print mfs
    with open('compiled_all.txt', 'r') as all_words_file,\
         open('mfs_answers.txt', 'w+') as result_file:
         
        lexelt_counts = defaultdict(lambda : 0)
        for line in all_words_file:
            reconstruct_tokens = []
            for token in line.split():
                if '#' not in token:
                    reconstruct_tokens.append(token)
                    continue
                target_word = token.split('#')[0]
                reconstruct_tokens.append(target_word)

            
            

            line = ' '.join(reconstruct_tokens)

            answer = mfs[target_word]

            instance_id = target_word + '.' + str(lexelt_counts[target_word])
            result_file.write( "docsrc " + instance_id + " " + str(answer)  + '\n')
            lexelt_counts[target_word] += 1
            
