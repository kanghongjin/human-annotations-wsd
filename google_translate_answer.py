"""Simple command-line example for Translate.
Command-line application that translates some text.

Taken from Joe Gregorio
"""
from __future__ import print_function

__author__ = 'jcgregorio@google.com (Joe Gregorio)'

from googleapiclient.discovery import build


def translate(queries):

    # Build a service object for interacting with the API. Visit
    # the Google APIs Console <http://code.google.com/apis/console>
    # to get an API key for your own application.
    service = build('translate', 'v2',
              developerKey='AIzaSyAhn6Y-J_sxUxt8Wz91dWjxuVLg9FtGG_o ')
    return service.translations().list(
        source='en',
        target='zh',
        q=queries
      ).execute()

if __name__ == '__main__':
  translate(['flower', 'car'])