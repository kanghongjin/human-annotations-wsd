import os
import re
import glob

from collections import Counter, defaultdict

def get_word_level_accuracy(filename):
    lemma_correct_answers = Counter()
    lemma_total = Counter()
    with open(filename, 'r') as answer_file:
        for line in answer_file:
            tokens = line.split(',')
            lemma = tokens[0].split('_')[0].strip()            

            is_correct = tokens[1].strip() == "correct"

            lemma_total[lemma] += 1 
            if is_correct:
                lemma_correct_answers[lemma] += 1

    lemma_to_accuracy = {}
    for lemma in lemma_total.keys():
        total_cases = lemma_total[lemma]
        
        lemma_to_accuracy[lemma] = float(lemma_correct_answers[lemma]) / total_cases

    

    return lemma_to_accuracy



if __name__ == "__main__":

    filenames = glob.glob('./analysis_at_word_level/*.csv')

    word_accuracy = defaultdict(list)

    print " , ",
    for filename in filenames:
        print filename.split('/')[-1] + ", ",

        word_level_accuracy = get_word_level_accuracy(filename)
        for lemma, accuracy in word_level_accuracy.iteritems():
            word_accuracy[lemma].append(accuracy)

    print ""

    for lemma, answers in word_accuracy.iteritems():
        print lemma, ",", 
        for answer in answers:
            print answer, ",",
        
        print ""